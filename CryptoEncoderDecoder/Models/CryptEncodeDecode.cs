﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CryptoEncoderDecoder.Models
{
    public class CryptEncodeDecode
    {
        public int Key { get; set; }
        public string Text { get; set; }
        public string NewText { get; set; }
        
    }
}