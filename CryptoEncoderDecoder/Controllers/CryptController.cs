﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Mvc;
using CryptoEncoderDecoder.Models;
using System.Web;
using System.Linq;

namespace CryptoEncoderDecoder.Views
{
    public class CryptController : Controller
    {
        //метод для дешифровки текста
        public string GetUnCryptText(string text, int key)
        {
            string alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
            var n = alphabet.Length;
            string outText = "";
            int u = 0;
            foreach (var textMini in text)
            {
                var isFind = false;
                for (var i = 0; i < n; i++)
                {
                    if (textMini == alphabet[i])
                    {
                        u = i;
                        isFind = true;
                        break;
                    }
                }
                if (isFind)
                {
                    var index = (u - key) % n;
                    if (index < 0) index += n;
                    outText += alphabet[index];
                }
                else
                {
                    outText += textMini;
                }
            }
            return outText;
        }
        public ActionResult CaesarDecrypt()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CaesarDecrypt(HttpPostedFileBase file, CryptEncodeDecode crypt)
        {
            ModelState.Clear();
            string inputText = crypt.Text;
            int key = crypt.Key;
            string inputTextNew = crypt.NewText;
            if (file != null && file.ContentLength > 0)
                try
                {
                    string path = Path.Combine(Server.MapPath("~/App_Data/uploads/"),
                                                Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    inputText = System.IO.File.ReadAllText(path);
                    crypt.Text = inputText;
                    ViewBag.Message = "Файл успешно был загружен!";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "Ошибка:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "Файл не выбран!";
            }
            //идет перебор всех вариантов и поиск по словарю, затем подсчитывается наибольшее количество совпадений слов в словаре, после чего этот вариант текста выводится 
            var fileText = System.IO.File.ReadAllLines(Server.MapPath("~/App_Data/uploads/vocabulary.txt"));
            List<string> str = new List<string>();
            Dictionary<int, string> dic = new Dictionary<int, string>();
            string[] strings = new string[33];
            var LogHashSet = new HashSet<string>(fileText);

            for (int i = 1; i < 33; i++)
            {
                str.Add(GetUnCryptText(inputText, i));
            }
            foreach (var item in str)
            {
                int count = 0;
                strings = item.Split(' ');
                for (int i = 0; i < strings.Length; i++)
                {
                    if (LogHashSet.Contains(strings[i]))
                    { count++; }
                }
                if (!dic.Keys.Contains(count))
                {
                    dic.Add(count, item);
                }
            }
            inputTextNew = dic.FirstOrDefault(x => x.Key == dic.Keys.Max()).Value;
            crypt.NewText = inputTextNew;
            return View("CaesarDecrypt", crypt);
        }

    
        public ActionResult CaesarCrypt()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CaesarCrypt(CryptEncodeDecode crypt)
        {
            ModelState.Clear();
            int key = crypt.Key;
            string inputText = crypt.Text;
            string inputTextNew = crypt.NewText;
            //шифрует текст
            string alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
            var n = alphabet.Length;
            int u = 0;
            foreach (var inputTextMini in inputText)
            {
                var isFind = false;
                for (var i = 0; i < n; i++)
                {
                    if (inputTextMini == alphabet[i])
                    {
                        u = i;
                        isFind = true;
                        break;
                    }
                }
                if (isFind)
                {
                    inputTextNew += alphabet[(u + key) % n];
                }
                else
                {
                    inputTextNew += inputTextMini;
                }
            }
            crypt.NewText = inputTextNew;
            return View("CaesarCrypt", crypt);
        }
    }
}